const mongoose = require('mongoose');
const orderSchema = require('../schemas/order');

orderSchema.statics = {
	create: function (data, cb) {
		let order = new this(data);
		order.save(cb);
	},

	get: function (query, cb) {
		this.find(query, cb);
	},

	update: function (query, updateData, cb) {
		this.findOneAndUpdate(query, { $set: updateData }, { new: true }, cb);
	},

	delete: function (query, cb) {
		this.findOneAndDelete(query, cb);
	}
}

const orderModel = mongoose.model('Order', orderSchema);
module.exports = orderModel;