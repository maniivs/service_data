const mongoose = require('mongoose');
const customerSchema = require('../schemas/customer');

customerSchema.statics = {
	create: function (data, cb) {
		let customer = new this(data);
		customer.save(cb);
    },
    
	delete: function (query, cb) {
		this.findOneAndDelete(query, cb);
	}
}

const customerModel = mongoose.model('Customer', customerSchema);
module.exports = customerModel;