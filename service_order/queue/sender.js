module.exports = function (data) {
    var q = 'orderUpdates';

    var open = require('amqplib').connect('amqps://fvdlerdc:7muf2R3ce8qgM9a7RcuZh5eDEtQaupLl@moose.rmq.cloudamqp.com/fvdlerdc?heartbeat=60');

    // Publisher
    open.then(function (conn) {
        return conn.createChannel();
    }).then(function (ch) {
        return ch.assertQueue(q).then(function (ok) {
            return ch.sendToQueue(q, Buffer.from(JSON.stringify(data)));
        });
    }).catch(console.warn);
}
