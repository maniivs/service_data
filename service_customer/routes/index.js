var Customers = require('../controlers/customer');

module.exports = function(router) {
    router.post('/customer', Customers.createCustomer);
    router.get('/customer', Customers.getCustomers);
    router.get('/customer/:name', Customers.getCustomer);
    router.put('/customer/:id', Customers.updateCustomer);
    router.delete('/customer/:id', Customers.removeCustomer);
}