const mongoose = require('mongoose');
const dotenv = require('dotenv');
dotenv.config();

module.exports = function () {

	mongoose.connect('mongodb+srv://root:pass1234@cluster0.kgttg.mongodb.net/customer', { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true, useFindAndModify: false });

	process.on('SIGINT', function () {
		mongoose.connection.close(function () {
			console.log('Mongoose disconnected on app termination');
			process.exit(0)
		});
	});
}