var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var customerSchema = new Schema({
	name: {
		type: String,
		unique: false,
		required: true
	},
	mobile: {
		type: String,
		unique: false,
		required: true
	},
	noOfOrders: {
		type: Number,
		unique: true,
		default: 0,
	}
}, {
	timestamps: true
});

module.exports = customerSchema;