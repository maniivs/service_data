import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from './material.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { CustomersComponent } from './customers/customers.component';
import { OrdersComponent } from './orders/orders.component';

import { CustomerService } from './_sevices/customer.services';
import { OrderService } from './_sevices/order.services';

@NgModule({
	declarations: [
		AppComponent,
		CustomersComponent,
		OrdersComponent
	],
	imports: [
		BrowserModule,
		HttpClientModule,
		AppRoutingModule,
		NoopAnimationsModule,
		FormsModule,
		ReactiveFormsModule,
		MaterialModule
	],
	providers: [
		CustomerService,
		OrderService
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
