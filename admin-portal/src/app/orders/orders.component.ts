import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { OrderService } from '../_sevices/order.services';
import { CustomerService } from '../_sevices/customer.services';

export interface OrderElement {
	customerId: string;
	price: number;
}

@Component({
	selector: 'app-orders',
	templateUrl: './orders.component.html',
	styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
	public spinner: Boolean = false;
	public orderForm: FormGroup;
	public customers: any[];

	public orderData = new MatTableDataSource<OrderElement>();
	displayedColumns: string[] = ['id', 'price'];

	constructor(
		private _orderService: OrderService,
		private _customerService: CustomerService
	) { }

	ngOnInit(): void {
		this._customerService.getCustomers().subscribe(
			(data: any) => {
				this.customers = data.customers;
			},
			(error) => {
			}
		)
		this._orderService.getOrders().subscribe(
			(data: any) => {
				this.orderData = new MatTableDataSource<OrderElement>(data.orders);
			},
			(error) => {
			}
		)
		this.orderForm = new FormGroup({
			customerId: new FormControl('', [Validators.required]),
			price: new FormControl('', [Validators.required]),
		});
	}

	submitOrderForm() {
		if (this.orderForm.valid) {
			let customerId = this.orderForm.value.customerId;
			let price = this.orderForm.value.price;
			console.log("price");
			this._orderService.createOrder({ customerId, price }).subscribe(
				(data: any) => {
					this._orderService.getOrders().subscribe(
						(data: any) => {
							this.orderData = new MatTableDataSource<OrderElement>(data.orders);
						},
						(error) => {
						}
					)
				},
				(error) => {
				}
			)
		}
	}

}
