import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { CustomerService } from '../_sevices/customer.services';

export interface CustomerElement {
	id: string;
	name: string;
	mobile: string;
	noOfOrders: number;
}


@Component({
	selector: 'app-customers',
	templateUrl: './customers.component.html',
	styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {
	public spinner: Boolean = false;
	public customerForm: FormGroup;

	public customerData = new MatTableDataSource<CustomerElement>();
	displayedColumns: string[] = ['id', 'name', 'mobile', 'noOfOrders'];

	constructor(
		private _customerService: CustomerService
	) { }

	ngOnInit(): void {
		this._customerService.getCustomers().subscribe(
			(data: any) => {
				this.customerData = new MatTableDataSource<CustomerElement>(data.customers);
			},
			(error) => {
			}
		)
		this.customerForm = new FormGroup({
			name: new FormControl('', [Validators.required]),
			mobile: new FormControl('', [Validators.required]),
		});
	}

	submitCustomerForm() {
		if (this.customerForm.valid) {
			let name = this.customerForm.value.name;
			let mobile = this.customerForm.value.mobile;
			this._customerService.createCustomer({ name, mobile }).subscribe(
				(data: any) => {
					this._customerService.getCustomers().subscribe(
						(data: any) => {
							this.customerData = new MatTableDataSource<CustomerElement>(data.customers);
						},
						(error) => {
						}
					)
				},
				(error) => {
				}
			)
		}
	}

}
