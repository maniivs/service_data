export interface Customer {
    mobile: number;
    name: string;
}